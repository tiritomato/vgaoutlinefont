**"VGA Outline"** is a free font which was copied from [Unicode VGA Font](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/).  
  
This font is outline (vector) font, and very useful to debugging, old-school games, or quick mock-up in recent GUI-Developing.
  
----
  
* Original Design: [Unicode VGA Font](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/) (XLisence)
* [VGA Outline License: MIT/X Lisence](https://sites.google.com/site/tirifarm/vgaoutline/license)